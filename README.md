# Introduction
This is the repo for the mod RimVore2 for the game Rimworld by Ludeon Studios.

We are currently in early development and thus offer no support if you install this mod and break your game.

The mod contains extreme fetish content, based on Vore. If you dislike the fetish or things associated with it, **do not install this mod**.

There is a public document that tracks the current development progress, take a look to get an overview of the mod: [docs.google.com](https://docs.google.com/document/d/1-0GeNCOOM0vKSZJHcXFJqD9onXWxc3GrSSap3jqmrEU/edit#)

Should you run into actual issues related to the mod, [provide a ticket](https://gitlab.com/Nabber/rimvore-2/-/issues) and the development team will take a look at it.