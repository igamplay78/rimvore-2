﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using RimWorld;

namespace RimVore2
{
    public interface IGenitalAccess
    {
        void AddSexualPart(Pawn pawn, RimVore2.SexualPart part);
        float GetSexAbility(Pawn pawn);
        Need GetSexNeed(Pawn pawn);
        bool HasBreasts(Pawn pawn);
        bool HasPenis(Pawn pawn);
        bool HasVagina(Pawn pawn);
        bool IsFertile(Pawn pawn);
        bool IsSexuallySatisfied(Pawn pawn);
        void PleasurePawn(Pawn pawn, float value);
    }
}
