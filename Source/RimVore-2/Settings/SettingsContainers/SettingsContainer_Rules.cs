﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace RimVore2
{


    public class SettingsContainer_Rules : SettingsContainer
    {
        public SettingsContainer_Rules() { }

        public Dictionary<string, VoreRulePreset> Presets = new Dictionary<string, VoreRulePreset>();
        private List<RuleEntry> rules;
        public List<RuleEntry> Rules
        {
            get
            {
                if(rules.EnumerableNullOrEmpty())
                {
                    rules = new List<RuleEntry>()
                    {
                        {
                            new RuleEntry(new RuleTarget(), new VoreRule(RuleState.On))
                        }
                    };
                }
                return rules;
            }
            set
            {
                NotifyStale();
                rules = value;
            }
        }
        public static List<Texture2D> ruleStateIcons;

        static readonly Func<VoreRule, RuleState> pathRulesStateGetter = (VoreRule rule) => rule.UseVorePathRules;
        static readonly Func<VoreRule, RuleState> considerMinimumAgeGetter = (VoreRule rule) => rule.ConsiderMinimumAge;
        static readonly Func<VoreRule, RuleState> allowedInAutoVoreGetter = (VoreRule rule) => rule.AllowedInAutoVore;
        static readonly Func<VoreRule, RuleState> useVorePathRulesGetter = (VoreRule rule) => rule.UseVorePathRules;
        static readonly Func<VoreRule, RuleState> canBeProposedToGetter = (VoreRule rule) => rule.CanBeProposedTo;
        static readonly Func<VoreRule, RuleState> canForceFailedProposalsGetter = (VoreRule rule) => rule.CanForceFailedProposals;
        static readonly Func<VoreRule, RuleState> canBeFOrcedFailedProposalsGetter = (VoreRule rule) => rule.CanBeForcedFailedProposals;
        static readonly Func<VoreRule, RuleState> canBeForcedIfDownedGetter = (VoreRule rule) => rule.CanBeForcedIfDowned;


        Dictionary<RV2DesignationDef, Func<VoreRule, RuleState>> designationGetters;
        Dictionary<RV2DesignationDef, Func<VoreRule, RuleState>> DesignationGetters
        {
            get
            {
                if(designationGetters == null)
                {
                    designationGetters = new Dictionary<RV2DesignationDef, Func<VoreRule, RuleState>>();
                    foreach(RV2DesignationDef des in DefDatabase<RV2DesignationDef>.AllDefsListForReading)
                    {
                        Func<VoreRule, RuleState> getter = (VoreRule rule) => rule.DesignationStates[des.defName];
                        designationGetters.SetOrAdd(des, getter);
                    }
                }
                return designationGetters;
            }
        }


        public void NotifyStale()
        {
            RuleCacheManager.Notify_AllPawnsStale();
            RV2_Patch_UI_Widget_GetGizmos.NotifyAllStale();
            RecacheAllDesignations();
            VoreInteractionManager.ClearCachedInteractions();
        }

        public void MoveRuleDown(int index)
        {
            Rules = Rules
                .Move(index, index + 1)
                .ToList();
        }

        public void MoveRuleUp(int index)
        {
            Rules = Rules
                .Move(index, index - 1)
                .ToList();
        }

        private void RecacheAllDesignations()
        {
            try
            {
                IEnumerable<PawnData> pawnDataList = SaveStorage.DataStore?.PawnData?.Values
                    .Where(pd => pd.IsValid);
                if(pawnDataList.EnumerableNullOrEmpty())
                {
                    RV2Log.Message("No pawn data to recache", true, false, "Designations");
                    return;
                }
                RV2Log.Message("Recaching pawndatas: " + string.Join(", ", pawnDataList.Select(pd => pd.Pawn?.LabelShort)), true, false, "Designations");
                foreach(PawnData pawnData in pawnDataList)
                {
                    RV2Log.Message($"Recaching designations for  {pawnData.Pawn?.LabelShort}", true, false, "Designations");
                    List<RV2DesignationDef> designations = RV2_Common.VoreDesignations;
                    if(designations.NullOrEmpty())
                    {
                        RV2Log.Error($"{pawnData.Pawn?.LabelShort}: No designationDefs to recache");
                        return;
                    }
                    foreach(RV2DesignationDef designation in designations)
                    {
                        VoreRule appliedRule = RuleCacheManager.GetFinalRule(pawnData.Pawn, RuleTargetRole.All, DesignationGetters[designation]);
                        if(appliedRule == null)
                        {
                            RV2Log.Message("Fatal error, no rule to recalculate designations with", true, true, "Designations");
                            return;
                        }
                        bool designationActive = DesignationGetters[designation](appliedRule) == RuleState.On;
                        RV2Log.Message($"{pawnData.Pawn?.LabelShort}: Set auto designation for {designation.label} to {designationActive}", true, true, "Designations");
                        pawnData.Designations[designation].enabledAuto = designationActive;
                    }
                }
            }
            catch(Exception e)
            {
                RV2Log.Error("The accursed RecacheAllDesignations fucked up. Pawn designations were not properly recached, reason: " + e);
            }
        }

        #region outside communication

        public bool DesignationActive(Pawn pawn, RV2DesignationDef designation)
        {
            RV2Log.Message("calculating enabled designation for " + pawn.LabelShort, true, true, "Designations");
            string key = designation.defName;
            VoreRule finalRule = RuleCacheManager.GetFinalRule(pawn, designation.assignedTo, DesignationGetters[designation]);
            // Log.Message(LogUtility.ToString(finalRule.DesignationStates));
            return finalRule.DesignationStates.TryGetValue(key) == RuleState.On;
        }
        public bool VorePathEnabled(Pawn pawn, RuleTargetRole role, VorePathDef path, bool isForAuto = false)
        {
            VoreRule rule = RuleCacheManager.GetFinalRule(pawn, role, useVorePathRulesGetter);
            VorePathRule pathRule = rule.GetPathRule(path.defName);
            bool enabled = pathRule.Enabled;
            if(isForAuto)
            {
                // Log.Message("Checking for auto-vore validity, is auto enabled? " + pathRule.AutoVoreEnabled);
                enabled &= pathRule.AutoVoreEnabled;
            }
            return enabled;
        }

        public ThingDef GetContainer(Pawn predator, VorePathDef pathDef)
        {
            VoreRule finalRule = RuleCacheManager.GetFinalRule(predator, RuleTargetRole.Predator, pathRulesStateGetter);
            VorePathRule pathRule = finalRule.GetPathRule(pathDef.defName);
            ThingDef container = pathRule.Container;
            if(container == RV2_Common.VoreContainerNone)
            {
                return null;
            }
            return container;
        }
        public bool HasValidAge(Pawn pawn, RuleTargetRole role = RuleTargetRole.All)
        {
            int requiredAge = GetValidAge(pawn, role);
            return pawn.ageTracker?.AgeBiologicalYears >= requiredAge;
        }

        public int GetValidAge(Pawn pawn, RuleTargetRole role = RuleTargetRole.All)
        {
            VoreRule activeRule = RuleCacheManager.GetFinalRule(pawn, role, considerMinimumAgeGetter);
            bool needsAgeCheck = activeRule.ConsiderMinimumAge == RuleState.On;
            if(!needsAgeCheck)
            {
                return -1;
            }
            return activeRule.MinimumAge;
        }

        public bool AllowedInAutoVore(Pawn pawn)
        {
            VoreRule activeRule = RuleCacheManager.GetFinalRule(pawn, RuleTargetRole.All, allowedInAutoVoreGetter);
            return activeRule.AllowedInAutoVore == RuleState.On;
        }
        public bool ShouldStruggle(Pawn prey, VorePathDef pathDef, bool isForced)
        {
            VoreRule finalRule = RuleCacheManager.GetFinalRule(prey, RuleTargetRole.Prey, useVorePathRulesGetter);
            VorePathRule pathRule = finalRule.GetPathRule(pathDef.defName);
            return pathRule.ShouldStruggle(isForced);
        }
        public int BaseRequiredStruggles(Pawn prey, VorePathDef pathDef)
        {
            VoreRule finalRule = RuleCacheManager.GetFinalRule(prey, RuleTargetRole.Prey, useVorePathRulesGetter);
            VorePathRule pathRule = finalRule.GetPathRule(pathDef.defName);
            return pathRule.RequiredStruggles;
        }

        public CorpseProcessingType GetCorpseProcessingType(Pawn pawn, VorePathDef path)
        {
            VoreRule finalRule = RuleCacheManager.GetFinalRule(pawn, RuleTargetRole.Predator, (VoreRule rule) => rule.UseVorePathRules);
            string pathKey = path.defName;
            if(finalRule != null)
            {
                VorePathRule pathRule = finalRule.GetPathRule(pathKey);
                return pathRule.CorpseProcessingType;
            }

            QuirkManager quirkManager = pawn.QuirkManager(false);
            if(quirkManager?.HasSpecialFlag("DestroyPawn") == true)
            {
                return CorpseProcessingType.Destroy;
            }

            return CorpseProcessingType.Dessicate;
        }

        public bool CanBeProposedTo(Pawn pawn, RuleTargetRole role = RuleTargetRole.All)
        {
            VoreRule activeRule = RuleCacheManager.GetFinalRule(pawn, role, canBeProposedToGetter);
            bool canBeProposedTo = activeRule.CanBeProposedTo == RuleState.On;
            return canBeProposedTo;
        }

        public bool CanForceFailedProposal(Pawn initiator, Pawn target, RuleTargetRole initiatorRole = RuleTargetRole.All, RuleTargetRole targetRole = RuleTargetRole.All)
        {
            VoreRule initiatorRule = RuleCacheManager.GetFinalRule(initiator, initiatorRole, canForceFailedProposalsGetter);
            VoreRule targetRule = RuleCacheManager.GetFinalRule(target, targetRole, canBeFOrcedFailedProposalsGetter);
            bool valid = initiatorRule.CanForceFailedProposals == RuleState.On && targetRule.CanBeForcedFailedProposals == RuleState.On;
            //Log.Message(initiator.LabelShort + " can vore " + target.LabelShort + " ? " + valid);
            return valid;
        }

        public bool CanBeForcedIfDowned(Pawn pawn, RuleTargetRole role = RuleTargetRole.All)
        {
            VoreRule activeRule = RuleCacheManager.GetFinalRule(pawn, role, canBeForcedIfDownedGetter);
            return activeRule.CanBeForcedIfDowned == RuleState.On;
        }

        #endregion

        public override void Reset()
        {
            rules = null;
            NotifyStale();
        }

        public override void ExposeData()
        {
            Scribe_Collections.Look(ref rules, "rules", LookMode.Deep);
            //ScribeUtilities.ScribeVariableDictionary(ref rules, "rules", LookMode.Deep, LookMode.Deep);
            ScribeUtilities.ScribeVariableDictionary(ref Presets, "presets", LookMode.Value, LookMode.Deep);
        }

        public override void DefsLoaded()
        {
            ruleStateIcons = new List<Texture2D>()
            {
                UITextures.CheckOnTexture,
                UITextures.CheckOffTexture,
                UITextures.CopyTexture
            };
            foreach(RuleEntry entry in Rules)
            {
                entry.Rule.DefsLoaded();
            }
        }

        public override void EnsureSmartSettingDefinition()
        {
            // nothing to do here for rules settings
        }
    }
}
